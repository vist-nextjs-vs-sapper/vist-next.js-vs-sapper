# VIST: Next.js vs Sapper #



### What is VIST: Next.js vs Sapper? ###

**VIST** (**V**ersatile **I**nteractive **S**oftware **T**utorial): **Next.js vs Sapper** is a comparison between the features of the app frameworks 
**Next.js** & **Sapper** and an examination of their present & future roles in web development.

### What is Next.js? ###

**Next.js** is an open-source **React** development framework built on top of **Node.js** and written in 
**JavaScript**, **TypeScript**, and **Rust**.

### What is Sapper? ###

**Sapper** is a production-optimized web application framework based on **Svelte**, a UI utility that compiles components at build time to **JavasScript**

The word **Sapper** (**S**velte **app** mak**er**) is inspired by military engineers or “sappers” who perform building and maintenance under combat conditions..